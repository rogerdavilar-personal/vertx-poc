package io.vertx.guides.wiki.database;

import java.util.HashMap;

import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;

// ProxyGen is used to trigger the code generation of a proxy for clients of that service
@ProxyGen
public interface WikiDatabaseService {

	// Fluent is optional. Allows fluent interfaces where operations can be chained
	// by returning the service instance.
	// This is mostly useful for the code generator when the service shall be
	// consumed from other JVM Languages
	@Fluent
	WikiDatabaseService fetchAllPages(Handler<AsyncResult<JsonArray>> resultHandler);

	@Fluent
	WikiDatabaseService fetchPage(String name, Handler<AsyncResult<JsonObject>> resultHandler);

	@Fluent
	WikiDatabaseService createPage(String title, String markdown, Handler<AsyncResult<Void>> resultHandler);

	@Fluent
	WikiDatabaseService savePage(int id, String markdown, Handler<AsyncResult<Void>> resultHandler);

	@Fluent
	WikiDatabaseService deletePage(int id, Handler<AsyncResult<Void>> resultHandler);

	// It is a good practice that service interfaces provide static methods to
	// create instances of both
	// the actual service implementation and proxy for client code over the event
	// bus
	static WikiDatabaseService create(JDBCClient dbClient, HashMap<SqlQuery, String> sqlQueries,
			Handler<AsyncResult<WikiDatabaseService>> readyHandler) {
		return new WikiDatabaseServiceImpl(dbClient, sqlQueries, readyHandler);
	}

	static WikiDatabaseService createProxy(Vertx vertx, String address) {
		return new WikiDatabaseServiceVertxEBProxy(vertx, address);
	}
}
